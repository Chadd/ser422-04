package edu.asupoly.ser422.lab4;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Additional components needed for part 3
import org.apache.commons.dbcp2.BasicDataSource;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;

public class Option3ServiceImpl extends Lab4Service {
	
	// Our local instance of Apache DBCP
	// This gets configured after properties are loaded
	private BasicDataSource connPool;
	
	// Dr. Gary's queries as PreparedStatement strings
	private String pssQueryAll = "SELECT grade FROM Enrolled";
	private String pssQueryYear = "SELECT grade from Enrolled JOIN Student ON (Enrolled.sid=Student.id) WHERE year=?";
	private String pssQuerySubject = "SELECT grade from Enrolled JOIN Course ON (Enrolled.crsid=Course.id) WHERE subject=?";
	private String pssQueryYearSubject = "SELECT grade from (Student JOIN Enrolled ON (Student.id=Enrolled.sid)) JOIN Course ON (Enrolled.crsid=Course.id) WHERE year=? AND subject=?";
	
		
	private String __jdbcUrl    = null;
	private String __jdbcUser   = null;
	private String __jdbcPasswd = null;
	private String __jdbcDriver = null;

	public Option3ServiceImpl() throws Exception {
        Properties props = new Properties();
        try {
            InputStream propFile = this.getClass().getClassLoader().getResourceAsStream("lab4db.properties");
            props.load(propFile);
            propFile.close();
        }
        catch (IOException ie) {
            ie.printStackTrace();
            throw new Exception("Could not open property file");
        }

		__jdbcUrl    = props.getProperty("jdbc.url");
		__jdbcUser   = props.getProperty("jdbc.user");
		__jdbcPasswd = props.getProperty("jdbc.passwd");
		__jdbcDriver = props.getProperty("jdbc.driver");
		try {
			//Class.forName("org.postgresql.Driver");
			Class.forName(__jdbcDriver); // ensure the driver is loaded
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("*** Cannot find the JDBC driver");
			cnfe.printStackTrace();
			throw new Exception("Cannot initialize service from property file");
		}
		
		// Configure outr DBCP instance
		connPool = new BasicDataSource();
        connPool.setDriverClassName(__jdbcDriver);
        connPool.setUsername(__jdbcUser);
        connPool.setPassword(__jdbcPasswd);
        connPool.setUrl(__jdbcUrl);
        connPool.setMinIdle(5);
        connPool.setMaxIdle(10);

	}

	private CallableStatement selectQuery(Connection aConn, String year, String subject)
									throws SQLException {
		CallableStatement csReturnable = null;
		
		// Polymorphism sure could do this more elegantly
		if (year != null && subject != null) {
			csReturnable = aConn.prepareCall("{call computeyearandsubjectavg(?, ?)}");
			csReturnable.setInt(1, new Integer(year));
			csReturnable.setString(2, subject);
		} else if (year != null) {
			csReturnable = aConn.prepareCall("{call computeyearavg(?)}");
			csReturnable.setInt(1, new Integer(year));
		} else if (subject != null) {
			csReturnable = aConn.prepareCall("{call computesubjectavg(?)}");
			csReturnable.setString(1, subject);
		} else {
			csReturnable = aConn.prepareCall("{call computeallavg()}");
		}
		
		return csReturnable;
	}
	/**
       This is the main implementation required by the GradeStrategy interface
	 */
	public String calculateGrade(String year, String subject) {
		
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		
		try {
			conn = connPool.getConnection();
			if (conn != null) {
				cs = selectQuery(conn, year, subject);
				rs = cs.executeQuery();
			}
			rs.next();
			String grade = rs.getString(1);
			return grade;
		}
		catch (SQLException se) {
			System.out.println("*** Uh-oh! Database Exception");
			se.printStackTrace();
		}
		catch (Exception e) {
			System.out.println("*** Some other exception was thrown");
			e.printStackTrace();
		}
		finally {  // why nest all of these try/finally blocks?
			try {
				if (rs != null) { rs.close(); }
			} catch (Throwable t1) {
				t1.printStackTrace();
			}
			try {
				if (conn != null) { conn.close(); }
			}
			catch (Throwable t) {
				t.printStackTrace();
			}
		}
		
		return "E";
	}
}
