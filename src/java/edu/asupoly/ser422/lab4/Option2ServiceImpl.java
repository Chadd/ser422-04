package edu.asupoly.ser422.lab4;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Additional components needed for part 2
import org.apache.commons.dbcp2.BasicDataSource;
import java.sql.PreparedStatement;

public class Option2ServiceImpl extends Lab4Service {
	
	// Our local instance of Apache DBCP
	// This gets configured after properties are loaded
	private BasicDataSource connPool;
	
	// Dr. Gary's queries as PreparedStatement strings
	private String pssQueryAll = "SELECT grade FROM Enrolled";
	private String pssQueryYear = "SELECT grade from Enrolled JOIN Student ON (Enrolled.sid=Student.id) WHERE year=?";
	private String pssQuerySubject = "SELECT grade from Enrolled JOIN Course ON (Enrolled.crsid=Course.id) WHERE subject=?";
	private String pssQueryYearSubject = "SELECT grade from (Student JOIN Enrolled ON (Student.id=Enrolled.sid)) JOIN Course ON (Enrolled.crsid=Course.id) WHERE year=? AND subject=?";
	
		
	private String __jdbcUrl    = null;
	private String __jdbcUser   = null;
	private String __jdbcPasswd = null;
	private String __jdbcDriver = null;

	public Option2ServiceImpl() throws Exception {
        Properties props = new Properties();
        try {
            InputStream propFile = this.getClass().getClassLoader().getResourceAsStream("lab4db.properties");
            props.load(propFile);
            propFile.close();
        }
        catch (IOException ie) {
            ie.printStackTrace();
            throw new Exception("Could not open property file");
        }

		__jdbcUrl    = props.getProperty("jdbc.url");
		__jdbcUser   = props.getProperty("jdbc.user");
		__jdbcPasswd = props.getProperty("jdbc.passwd");
		__jdbcDriver = props.getProperty("jdbc.driver");
		try {
			//Class.forName("org.postgresql.Driver");
			Class.forName(__jdbcDriver); // ensure the driver is loaded
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("*** Cannot find the JDBC driver");
			cnfe.printStackTrace();
			throw new Exception("Cannot initialize service from property file");
		}
		
		// Configure outr DBCP instance
		connPool = new BasicDataSource();
        connPool.setDriverClassName(__jdbcDriver);
        connPool.setUsername(__jdbcUser);
        connPool.setPassword(__jdbcPasswd);
        connPool.setUrl(__jdbcUrl);
        connPool.setMinIdle(5);
        connPool.setMaxIdle(10);

	}

	private PreparedStatement selectQuery(Connection aConn, String year, String subject)
									throws SQLException {
		PreparedStatement psReturnable = null;
		
		// Polymorphism sure could do this more elegantly
		if (year != null && subject != null) {
			psReturnable = aConn.prepareStatement(pssQueryYearSubject);
			psReturnable.setInt(1, new Integer(year));
			psReturnable.setString(2, subject);
		} else if (year != null) {
			psReturnable = aConn.prepareStatement(pssQueryYear);
			psReturnable.setInt(1, new Integer(year));
		} else if (subject != null) {
			psReturnable = aConn.prepareStatement(pssQuerySubject);
			psReturnable.setString(1, subject);
		} else {
			psReturnable = aConn.prepareStatement(pssQueryAll);
		}
		
		return psReturnable;
	}
	/**
       This is the main implementation required by the GradeStrategy interface
	 */
	public String calculateGrade(String year, String subject) {
		
		Connection conn = null;
		ResultSet rs = null;
		double grade = 0.0;
		PreparedStatement ps = null;
		
		try {
			conn = connPool.getConnection();
			if (conn != null) {
				ps = selectQuery(conn, year, subject);
				rs = ps.executeQuery();
			}
			
			int count = 0;
			double gradesum = -1.0;
			while (rs.next()) {
				gradesum += rs.getDouble(1);
				count++;
			}
			if (count > 0) {
				grade = gradesum / count;
			}
			return mapToLetterGrade(grade);
		}
		catch (SQLException se) {
			System.out.println("*** Uh-oh! Database Exception");
			se.printStackTrace();
			return mapToLetterGrade(grade);
		}
		catch (Exception e) {
			System.out.println("*** Some other exception was thrown");
			e.printStackTrace();
			return mapToLetterGrade(grade);
		}
		finally {  // why nest all of these try/finally blocks?
			try {
				if (rs != null) { rs.close(); }
			} catch (Throwable t1) {
				t1.printStackTrace();
			}
			try {
				if (conn != null) { conn.close(); }
			}
			catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}
}
