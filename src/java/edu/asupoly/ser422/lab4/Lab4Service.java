package edu.asupoly.ser422.lab4;

import java.io.InputStream;
import java.util.Properties;

public abstract class Lab4Service {
	// This is what you need to implement in a subclass!
	protected abstract String calculateGrade(String year, String subject);

	public final String mapToLetterGrade(double grade) {
		if (grade >= 98.0) return "A+";
		if (grade >= 93.0) return "A";
		if (grade >= 90.0) return "A-";
		if (grade >= 88.0) return "B+";
		if (grade >= 83.0) return "B";
		if (grade >= 80.0) return "B-";
		if (grade >= 77.0) return "C+";
		if (grade >= 70.0) return "C";
		if (grade >= 60.0) return "D";
		if (grade < 0.0) return "I";
		return "E";
	}
	public static Lab4Service getService() {
		return __service;
	}
	private static Lab4Service __service = null;

	static {
		Properties props = new Properties();
		try {
			InputStream propFile = Lab4Service.class.getClassLoader().getResourceAsStream("lab4.properties");
			props.load(propFile);
			propFile.close();

			String serviceClass = props.getProperty("service.class");
			Class initClass = null;
			if (serviceClass != null) {
				initClass = Class.forName(serviceClass);
			} else {
				throw new Exception("Could not initialize service class");
			}
			__service = (Lab4Service)initClass.newInstance();
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}
}
