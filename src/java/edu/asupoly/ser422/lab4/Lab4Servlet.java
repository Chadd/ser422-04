/*
 * Lab4Servlet.java, created on Feb 7, 2008
 *
 * Copyright:  �2008 Kevin A. Gary All Rights Reserved
 *
 */
package edu.asupoly.ser422.lab4;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @author Kevin Gary
 *
 */
public class Lab4Servlet extends HttpServlet {

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest arg0, HttpServletResponse arg1)
	throws ServletException, IOException {
		long t1 = System.currentTimeMillis();
		StringBuffer pageBuf = new StringBuffer();
		String grade = "";
		String year = arg0.getParameter("year");
		String subject = arg0.getParameter("subject");
		
		if (year != null) {
			pageBuf.append("  Year: " + year);
		}
		if (subject != null) {
			pageBuf.append("  Subject: " + subject);
		}
		
		Lab4Service service = Lab4Service.getService();
		if (service == null) {
			pageBuf.append("\tSERVICE NOT AVAILABLE");
		} else {
			grade = service.calculateGrade(year, subject);
			pageBuf.append("\tGrade: " + grade);
		}
		
		// some generic setup - our content type and output stream
		arg1.setContentType("text/plain");
		PrintWriter out = arg1.getWriter();

		// System.out.println will go to catalina.out. Pretty lame, should use a logger
		System.out.println("QUERY RESULTS: ");
		System.out.println(pageBuf.toString());
		System.out.println("\tProcessing took " + (System.currentTimeMillis() - t1));
		out.println(grade);
	}
}
