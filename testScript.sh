#!/bin/bash

cat /dev/null > testOutput.txt
while read urls; do
  PTIME=""
  wget -q -O /dev/null $urls
  PTIME=`tail -n 1 /var/log/tomcat7/catalina.out`
  PTIME=`echo $PTIME | sed 's/Processing took//'`
  echo $PTIME | sed 's/^ *//;s/ *$//' >> testOutput.txt
done < test.urls
